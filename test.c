#include "backtrace.h"

void
f8(void)
{
    backtrace();
}

void f7(void) { f8(); }
void f6(void) { f7(); }
void f5(void) { f6(); }
void f4(void) { f5(); }
void f3(void) { f4(); }
void f2(void) { f3(); }
void f1(void) { f2(); }

int
main(int argc, char *argv[])
{
    f1();
    return 0;
}
