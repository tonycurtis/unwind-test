#include <stdio.h>
#include <string.h>

#define UNW_LOCAL_ONLY
#include <libunwind.h>

#define MAX_PROC_NAME_LEN 64

#define INDENT_PER_LEVEL 2

void
backtrace(void)
{
    unw_context_t u_ctxt;
    unw_cursor_t u_crsr;
    unsigned int indent = 0;

    unw_getcontext(&u_ctxt);

    unw_init_local(&u_crsr, &u_ctxt);

    while (unw_step (&u_crsr) > 0) {
        unw_word_t ip, sp;
        char procname[MAX_PROC_NAME_LEN];
        unw_word_t off;
        int s;

        s = unw_get_reg(&u_crsr, UNW_REG_IP, &ip);
        if (s != 0) {
            ip = 0L;
        }
        s = unw_get_reg(&u_crsr, UNW_REG_SP, &sp);
        if (s != 0) {
            sp = 0L;
        }

        s = unw_get_proc_name(&u_crsr, procname, MAX_PROC_NAME_LEN, &off);
        /*
         * say something sensible if we can't find the procedure name
         * (e.g. in a stripped executable)
         */
        if (s != 0) {
            strncpy(procname, "(none)", MAX_PROC_NAME_LEN);
        }

        if (indent > 0) {
            fprintf(stderr, "%*c", indent, ' ');
        }
        fprintf(stderr,
                "%s: ip = %#lx, sp = %#lx\n",
                procname,
                (long) ip, (long) sp
                );

        indent += INDENT_PER_LEVEL;
    }
}
