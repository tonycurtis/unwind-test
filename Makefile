CC = gcc
CFLAGS = -ggdb
LD = $(CC)
LDFLAGS =
LIBS = -lunwind

EXE = a.out

SOURCES = $(wildcard *.c)
OBJECTS = $(SOURCES:.c=.o)

.PHONY:		all	tidy	clean

all:	$(EXE)

$(EXE):	$(OBJECTS)
	$(LD) $(LDFLAGS) -o $@ $^ $(LIBS)

tidy:
	rm -f $(OBJECTS)

clean:	tidy
	rm -f $(EXE)
